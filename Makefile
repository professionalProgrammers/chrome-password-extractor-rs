RELEASE_DIR = ./target/release

PKG_EXE_NAME = chrome-password-extractor-pkg.exe
PKG_EXE = $(RELEASE_DIR)/$(PKG_EXE_NAME)

export CARGO_HOME = $(USERPROFILE)/.cargo
export RUSTFLAGS = --remap-path-prefix $(CARGO_HOME)/registry/src=src -C target-feature=+crt-static -C link-arg=/DEBUG:NONE

.PHONY: clean pkg

clean:
	cd $(RELEASE_DIR) && del /f $(PKG_EXE_NAME)

pkg:
	cargo build --release
	upx --best --ultra-brute $(RELEASE_DIR)/chrome-password-extractor.exe -o $(PKG_EXE)