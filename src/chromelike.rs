use crate::BaseDir;
use std::{collections::HashMap, path::Path};

/// LocalState
#[derive(Debug, serde::Deserialize)]
pub struct LocalState {
    /// os crypt
    pub os_crypt: OsCrypt,

    /// Unknown data
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// os_crypt filed of LocalState
#[derive(Debug, serde::Deserialize)]
pub struct OsCrypt {
    /// The encrypted key
    pub encrypted_key: String,

    /// Unknown data
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// Login Data
#[derive(Debug)]
pub struct LoginData {
    /// Url origin
    pub origin_url: String,
    /// Submit url?
    pub action_url: String,
    /// The username element
    pub username_element: String,
    /// The username value
    pub username_value: String,
    /// The password element
    pub password_element: String,
    /// The password value
    pub password_value: Vec<u8>,
    /// The submit element
    pub submit_element: String,
    /// ?
    pub signon_realm: String,
}

/// A chrome-like target
#[derive(Debug)]
pub struct ChromeLikeTarget {
    /// Target name
    name: &'static str,
    /// Path base
    base: BaseDir,
    /// app dir
    app_dir: &'static str,
}

impl ChromeLikeTarget {
    /// Make a new [`ChromeLikeTarget`]
    pub const fn new(name: &'static str, base: BaseDir, app_dir: &'static str) -> Self {
        Self {
            name,
            base,
            app_dir,
        }
    }

    /// Get the name
    pub fn name(&self) -> &str {
        self.name
    }

    /// Get the base dir
    pub fn base(&self) -> &BaseDir {
        &self.base
    }

    /// Get the app dir
    pub fn app_dir(&self) -> &Path {
        &self.app_dir.as_ref()
    }
}

/// Google chrome
pub const CHROME_TARGET: ChromeLikeTarget =
    ChromeLikeTarget::new("Chrome", BaseDir::DataLocal, "Google\\Chrome");

/// Chrome canary
pub const CHROME_CANARY_TARGET: ChromeLikeTarget =
    ChromeLikeTarget::new("Chrome Canary", BaseDir::DataLocal, "Google\\Chrome SxS");

/// Brave
pub const BRAVE_TARGET: ChromeLikeTarget =
    ChromeLikeTarget::new("Brave", BaseDir::DataLocal, "BraveSoftware\\Brave-Browser");

/// Yandex
pub const YANDEX_TARGET: ChromeLikeTarget =
    ChromeLikeTarget::new("Yandex", BaseDir::DataLocal, "Yandex\\YandexBrowser");

/// Chromelike targets
pub const CHROMELIKE_TARGETS: &[&ChromeLikeTarget] = &[
    &CHROME_TARGET,
    &CHROME_CANARY_TARGET,
    &BRAVE_TARGET,
    &YANDEX_TARGET,
];
